from copy import deepcopy

from . import db


class Model(db.Model):
    __abstract__ = True

    @classmethod
    def create(cls, **kwargs):
        instance = cls(**kwargs)
        db.session.add(instance)
        db.session.commit()
        return instance

    def update(self, **kwargs):
        try:
            for key in kwargs:
                setattr(self, key, kwargs[key])
            db.session.commit()
            return self
        except KeyError as e:
            db.session.rollback()
            raise e

    def delete(self, **kwargs):
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            raise e

        return True

