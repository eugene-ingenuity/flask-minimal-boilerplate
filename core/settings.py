import os

BASE_DIR = os.path.abspath(os.path.dirname(__name__))

class BaseConfig:
    SECRET_KEY = os.getenv('SECRET_KEY', 'some_random_string')
    DEBUG = False

class DevelopmentConfig(BaseConfig):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = f'sqlite:///{os.path.join(BASE_DIR, "development.db")}'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    RESTPLUS_MASK_SWAGGER = False


class ProductionConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = f'sqlite:///{os.path.join(BASE_DIR, "production.db")}'


config = dict(
    dev=DevelopmentConfig,
    prod=ProductionConfig
)
