import os

from app import blueprint
from core import create_app, db
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

app = create_app(os.getenv('APP_ENV', 'dev'))
app.register_blueprint(blueprint)
app.app_context().push()

"""
Instantiate the manager and migrate classes by passing
the app instance to their respective constructors
"""
manager = Manager(app)
migrate = Migrate(app, db)

"""
Pass 'db' and MigrateCommand to the manager.add_command
to expose all database migration commands through
Flask-Script

We can execute the command via
$ python manage.py db <command>

e.g.
$ python manage.py db init

Where db is the one we passed to the manager.add_command
And migrate is a part of the MigrateCommand
"""
manager.add_command('db', MigrateCommand)

"""
The same with `manager.add_command` but we are using the
decorator method.

By executing `python manage.py runserver` in our command line,
it immediately executes the `runserver()` function.
"""
@manager.command
def runserver():
    app.run()


if __name__ == '__main__':
    manager.run()
