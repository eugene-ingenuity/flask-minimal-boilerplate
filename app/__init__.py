from flask import Blueprint
from flask_restplus import Api

blueprint = Blueprint('api', __name__)

api = Api(blueprint, title='Flask API', version='1.0', description='Minimal Flask API')

# add custom namespaces here.
# api.add_namespace(my_custom_namespace, path='/custom_path')
